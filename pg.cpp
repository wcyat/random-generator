#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <chrono>
using namespace std;
string read(string value) {
    ifstream input("pg_options.txt");
    int olength;
    string line = "", output = "";
    int i = 0;
    if (!input.is_open()) {
        cout << "pg_options.txt missing.";
        return "error";
    }
    while (getline(input, line)) {
        size_t pos = line.find(value);
        if (pos != string::npos) {
            while (true) {
                if (line[i] == '=') {
                    i++;
                    break;
                }
                i++;
            }
            olength = line.length() - value.length() - 1;
            for (int i2 = 0; i2 < olength; i2++) {
                output += line[i];
                i++;
            }
        }
    }
    return output;
}

bool checkint(string word) {
    bool has_only_digits = (word.find_first_not_of("0123456789") == string::npos);
    return has_only_digits;
}

char grand(std::minstd_rand simple_rand, int size, char x[]) {
    int i = simple_rand() % size;
    return x[i];
}

void generate() {
    char uppercasec[26] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' }, lowercasec[26] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }, specialc[7] = { '!', '#', '$', '%', '&', '*', '?' }, numbersc[10] = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
    minstd_rand simple_rand;
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    int time = std::chrono::duration_cast<std::chrono::nanoseconds>(t1.time_since_epoch()).count(), digits = 0, upper = 0, lower = 0, special = 0, numbers = 0, l, amount[4] = { 0 };
    simple_rand.seed(time);
    digits = stoi(read("digits"));
    double ld[3] = {2, 1.5, 2};
    string readitems[4] = { "include_upper_case", "include_lower_case", "include_special_characters", "include_numbers" };
    bool include[4] = { false };
    for (int i = 0; i < 4; i++) {
        if (read(readitems[i]) == "true") {
            include[i] = true;
            amount[i] = 1;
            digits--;
        }
    }
    int addamount;
    for (int i = 0; i < 3; i++) {
        if (include[i]) {
            l = digits / ld[i];
            if (!(l < 1)) {
                addamount = simple_rand() % l;
                amount[i] += addamount;
                digits -= addamount;
            }
        }
    }
    if (include[3]) {
        amount[3] += digits;
    }
    else {
        for (int i = 0; i < 3; i++) {
            if (include[i] == true) {
                amount[i] += digits;
            }
        }
    }
    for (digits = stoi(read("digits")); digits > 0; digits--) {
        int i1 = simple_rand() % 4;
        if (amount[i1] >= 1) {
            char g[4] = { grand(simple_rand, 26, uppercasec), grand(simple_rand, 26, lowercasec),  grand(simple_rand, 7, specialc), grand(simple_rand, 10, numbersc) };
            cout << g[i1];
            amount[i1]--;
        }
        else {
            digits++;
        }
    }
}

int main()
{
    generate();
}
